import React, { Component } from 'react';
import './FullPost.css';
import axios from 'axios';

class FullPost extends Component{

    state = {
        post: {}
    };

    componentDidMount(){
        console.log('id: ' + this.props.match.params.id);
        if (this.props.match.params.id && (!this.state.post || (this.state.post && this.state.post.id !== this.props.match.params.id))) {
            axios.get('/posts/' + this.props.match.params.id)
                .then( post => {
                    console.log(post);

                    this.setState({
                        post: post.data
                    })
                });
        }
    }

    render(){
        let post = <p style={{textAlign: 'center'}}>Please select a post</p>;

        if (this.props.match.params.id){
            post = (
                <div>
                    <h4>{this.state.post.title}</h4>
                    <p>{this.state.post.body}</p>
                    <button>Delete</button>
                </div>
                );
        }

        return(
            <div className="FullPost">
                {post}
            </div>
        );
    }
}

export default FullPost;