import React, { Component } from 'react';
import './Post.css';
import { Link } from 'react-router-dom';

class Post extends Component{
    render(){
        return(
            <Link to={'/posts/' + this.props.id}>
                <div className="Post" onClick={() => this.props.clicked(this.props.id)}>
                    <h4>{this.props.title}</h4>
                    <p>{this.props.body}</p>
                    <h5 style={{'textAlign':'right'}}>{this.props.author}</h5>
                </div>
            </Link>
        );
    }
}

export default Post;