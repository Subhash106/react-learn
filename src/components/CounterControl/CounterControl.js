import React, { Component } from 'react';
import './CounterControl.css';

class CounterControl extends Component{
    render(){
        return(
            <div className="CounterControl">
                <button onClick={this.props.clicked}>{this.props.label}</button>
            </div>
        );
    }
}

export default CounterControl;