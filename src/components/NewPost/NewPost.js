import React, { Component } from 'react';
import axios from 'axios';
import './NewPost.css';
import Input from '../UI/Input/Input';

class NewPost extends Component{
    state = {
        postForm: {
            title: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Title'
                },
                value: ''
            },
            content: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Content'
                },
                value: ''
            },
            authorName: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        {value: 'subhash', displayValue: 'Subhash'},
                        {value: 'suresh', displayValue: 'Suresh'},
                        {value: 'rubesh', displayValue: 'Rubesh'},
                    ]
                },
                value: ''
            },
            authorEmail: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Email'
                },
                value: ''
            }
        }
    };

    storeDataHandler = () => {
        console.log('storeDataHandler called');

        const postForm = this.state.postForm;
        const data = {
            title: postForm.title.value,
            body: postForm.content.value,
            authorName: postForm.authorName.value,
            authorEmail: postForm.authorEmail.value
        };

        axios.post('https://jsonplaceholder.typicode.com/posts', data)
            .then(response => {
                console.log(response);
            });
    }

    inputChangedHandler = (event, elementIdentifier) => {
        const postForm = {...this.state.postForm};
        const element = {...postForm[elementIdentifier]};
        element.value = event.target.value;
        postForm[elementIdentifier] = element;
        this.setState({
            postForm: postForm
        });
    }

    render(){

        let elementsArray = [];

        for(let key in this.state.postForm){
            elementsArray.push({
                id: key,
                config: this.state.postForm[key]
            });
        }

        return (
            <div className="NewPost">
                <h2>Add a post</h2>

                {elementsArray.map(element => <Input changed={(event) => this.inputChangedHandler(event, element.id)} key={element.id} elementType={element.config.elementType} elementConfig={element.config.elementConfig} value={element.config.value} />)}

                <button onClick={this.storeDataHandler}>Save</button>
            </div>
        );
    }
}

export default NewPost;