import React, { Component } from 'react';
import CounterControl from '../CounterControl/CounterControl';
import CounterOutput from '../CounterOutput/CounterOutput';
import './Counter.css';
import { connect } from 'react-redux';

class Counter extends Component{
    render(){
        return(
            <div className="Counter">
                <section>
                    <CounterOutput counter={this.props.ctr} />
                </section>
                <section>
                    <CounterControl label="Increment" clicked={this.props.onIncrementCounter} />
                    <CounterControl label="Decrement" clicked={this.props.onDecrementCounter}/>
                    <CounterControl label="Add 5" clicked={this.props.addFiveHandler} />
                    <CounterControl label="Subtract 5" clicked={this.props.subtractFiveHandler} />
                </section>
                <section>
                    <button onClick={() => this.props.storeResultHandler(this.props.ctr)}>Store Result</button>
                    <ol>
                        { this.props.counterResults.map((result, index) => <li key={index} onClick={() => this.props.deleteResultHandler(this.props.ctr)}>{result}</li>) }
                    </ol>
                </section>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        ctr: state.ctr.counter,
        counterResults: state.res.results
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onIncrementCounter: () => dispatch({type: 'INCREMENT'}),
        onDecrementCounter: () => dispatch({type: 'DECREMENT'}),
        addFiveHandler: () => dispatch({type: 'ADD_FIVE', value: 5}),
        subtractFiveHandler: () => dispatch({type: 'SUBTRACT_FIVE', value: 5}),
        storeResultHandler: (counter) => dispatch({type: 'STORE_RESULT', counter: counter}),
        deleteResultHandler: (counter) => dispatch({type: 'DELETE_RESULT', counter: counter})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);