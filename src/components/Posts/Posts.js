import React, { Component } from 'react';
import './Posts.css';
import Post from '../Post/Post';

class Posts extends Component{
    render(){
        return(
            <div className="Posts">
                {this.props.posts.map( post => <Post key={post.id} title={post.title} body={post.body} author={post.author} id={post.id} clicked={() => this.props.clicked(post.id)} />)}
            </div>
        )
    }
}

export default Posts;