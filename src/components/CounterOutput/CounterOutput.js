import React, { Component } from 'react';
import './CounterOutput.css';

class CounterOutput extends Component{
    render(){
        return(
            <div className="CounterOutput">
                <p>Current Counter: {this.props.counter}</p>
            </div>
        );
    }
}

export default CounterOutput;