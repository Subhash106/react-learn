const initialState = {
    results: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case ('STORE_RESULT') :
            const results = [...state.results];
            results.push(action.counter);

            return {
                ...state,
                results: results
            }
        case ('DELETE_RESULT') :
            const updatedResults = [...state.results];
            const index = updatedResults.indexOf(action.counter);
            if (index > -1) {
                updatedResults.splice(index, 1);
            }

            return {
                ...state,
                results: updatedResults
            }
    }

    return state;
}

export default reducer;