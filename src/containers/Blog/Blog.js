import React, { Component } from 'react';
import Posts from '../../components/Posts/Posts';
import './Blog.css';
import FullPost from '../../components/FullPost/FullPost';
import axios from 'axios';
import NewPost from '../../components/NewPost/NewPost';
import { Route, NavLink } from 'react-router-dom';
import Counter from '../../components/Counter/Counter';

class Blog extends Component{
    state = {
        posts: [],
        selectedPostId: null,
        error: false
    };

    componentDidMount(){
        axios.get('/posts')
            .then(response => {
                console.log(response);
                const posts = response.data.slice(0, 4);
                const updatedPosts = posts.map( post => {
                    return {
                        ...post,
                        author: 'Max'
                    }
                });

                this.setState({
                    posts: updatedPosts
                })
            }).catch(error => {
                console.log(error);
                this.setState({error: true});
            });
    }

    postClickedHandler = (id) => {
        console.log(id);

        this.setState({
            selectedPostId: id
        });
    }

    render(){
        let posts = <p style={{textAlign: 'center', color: 'red'}}>Something went wrong!</p>;

        if (!this.state.error) {
            posts = <Posts clicked={(id) => this.postClickedHandler(id)} posts={this.state.posts} />;
        }

        return(
            <div className="Blog">
                <header>
                    <ul>
                        <li>
                            <NavLink to="/" exact >Home</NavLink>
                        </li>
                        <li>
                            <NavLink to={{
                                pathname:"/new-post",
                                hash:"#submit",
                                search:"?quick-submit=true"
                            }}>New Post</NavLink>
                        </li>
                        <li>
                            <NavLink to="/counter" exact >Counter</NavLink>
                        </li>
                    </ul>
                </header>
                <Route path="/" exact render={() => posts} />
                <Route path="/new-post" component={NewPost} />
                <Route path="/counter" exact component={Counter} />
                <Route path="/posts/:id" exact component={FullPost} />
            </div>
        );
    }
}

export default Blog;